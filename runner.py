import os

from dotenv import load_dotenv

from census import CensusController

load_dotenv()
api_key = os.getenv("CENSUS_API_KEY")

c = CensusController(api_key)
tables = c.list_tables(2021, 25)
print(tables)
