import pandas as pd


class CensusController:
    def __init__(self, api_key):
        self.api_key = api_key

    def list_tables(
        self, census_year: int, acs_type: int | None = None
    ) -> pd.DataFrame:
        """
        Queries the Census API and grabs all available ACS Tables for the `census_year` and `acs_type`.
        Then, returns a DataFrame of the `Table ID`, `Table Title`, `Table Universe`, `Data Product Type`, and `Year`.
        Parameters:
          census_year- the desired year of data.
          acs_type- denotes whether to return the ACS 1-year or 5-year estimates. By default, returns both.
          Value is either  1 or 5.
        """
        df = pd.read_excel(
            f"https://www2.census.gov/programs-surveys/acs/tech_docs/table_shells/table_lists/{census_year}_DataProductList.xlsx"
        )
        df = df[
            ["Table ID", "Table Title", "Table Universe", "Data Product Type", "Year"]
        ].astype(str)

        if acs_type:
            if acs_type != 1 and acs_type != 5:
                raise Exception(
                    "Invalid input for `acs_type`. It must be either 1 or 5."
                )
            df = df[df["Year"].str.contains(str(acs_type))]
        return df.reset_index(drop=True)

    def run(self):
        print("running!")

        return None
